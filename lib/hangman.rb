class Hangman
  attr_reader :guesser, :referee, :board
  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def play
    setup
    take_turn until over?
    conclude
  end



  def setup
    sec_length = @referee.pick_secret_word
    @guesser.register_secret_length(sec_length)
    @board = Array.new(sec_length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  private

  def update_board(index, ch)
    index.each { |idx| @board[idx] = ch }
  end

  def over?
    @board.none? { |val| val == nil }
  end

  def conclude
    puts "Congratulations! The word was #{@board.join("")}"
  end

end

class HumanPlayer

  def initialize
    @guessed_letter = []
  end

  def guess(board)
    print_board(board)
    print ">"
    guess = gets.chomp
    process_guess(guess)
  end

  def handle_response(ch, indices)

  end

  def pick_secret_word
    puts "Think of a secret word"
    print "Enter it's length > "
    gets.chomp.to_i
  end

  def check_guess(letter)
    puts "The computer has guessed the letter #{letter}"
    print "Positions of word (ex 0 2 4):"
    answer = gets.chomp.split.map(&:to_i)
  end

  def register_secret_length(length)
    puts "The secret word has length #{length}"
  end

  private

  def process_guess(guess)
    unless @guessed_letter.include?(guess)
      @guessed_letter << guess
      return guess
    else
      puts "You already guessed #{guess}"
      puts "You have alread guessed #{@guessed_letter}"
      puts "Please guess again"
      print ">"
      guess = gets.chomp
      process_guess(guess)
    end
  end

  def print_board(board)
    board_s = board.map do |ch|
      ch.nil? ? "_" : ch
    end.join("")
    puts "Secret word: #{board_s}"
  end
end

class ComputerPlayer
  attr_accessor :dictionary, :candidate_words

  def initialize(dictionary = File.readlines("lib/dictionary.txt").map(&:chomp))
    @dictionary = dictionary
    @alphabet = ('a'..'z').to_a
    @candidate_words = nil
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(ch)
    @secret_word.chars.map.with_index do |letter, idx|
      idx if ch == letter
    end.compact
  end

  def register_secret_length(length)
    @secret_length = length
    @candidate_words = @dictionary.select! { |word| word.length == @secret_length }
  end


  def handle_response(ch, indices)
    array = []
    @dictionary.each do |word|
      array << word if indices.empty? && !word.include?(ch)
      indices.all? do |idx|
        array << word if word[idx] == ch && index_match(word, ch) == indices
      end
    end
    @candidate_words = array.uniq
  end



  def guess(board)
    array = []
    count_hash = Hash.new(0)
    @dictionary.join.chars.each do |ch|
      count_hash[ch] += 1
    end
    sort_array = count_hash.sort_by { |k, v| v }
    sort_array.each do |subarray|
      array << subarray[0]
    end
    array = array.reverse
    counter = 0
    until over(board)
      unless board.include?(array[counter])
        return array[counter]
      else
        counter += 1
      end
    end
  end

private

def index_match(word, ch)
  array = []
  word.chars.each_with_index do |char, idx|
    array << idx if char == ch
  end
  array
end

def over(array)
  array.none? { |val| val == nil }
end
  if __FILE__ == $PROGRAM_NAME
    players = {
      guesser: ComputerPlayer.new,
      referee: HumanPlayer.new
    }
    game = Hangman.new(players)
    game.play
  end

end
